using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MopubAdsManager: MonoBehaviour
{
    static private MopubAdsManager instance = null;
    public static MopubAdsManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject obj = new GameObject();
                obj.name = "MopubAdsManager";
                instance = obj.AddComponent<MopubAdsManager>();
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }

#if UNITY_IOS
    private readonly string[] _bannerAdUnits        = { "0ac59b0996d947309c33f59d6676399f" };
    private readonly string[] _interstitialAdUnits  = { "4f117153f5c24fa6a3a92b818a5eb630" };
    private readonly string[] _rewardedAdUnits      = { "8f000bd5e00246de9c789eed39ff6096", "98c29e015e7346bd9c380b1467b33850" };
#elif UNITY_ANDROID || UNITY_EDITOR
    //private readonly string[] _bannerAdUnits        = { "3a61d61733814f5fb5e7a35d46622183" };
    //private readonly string[] _interstitialAdUnits  = { "1e4ba009729d4f59b77130708127954c" };
    //private readonly string[] _rewardedAdUnits      = { "7e756ab79a29438f87bc5c6d6032d31f", "7e756ab79a29438f87bc5c6d6032d31f" };
    private readonly string[] _bannerAdUnits        = { "b195f8dd8ded45fe847ad89ed1d016da" };
    private readonly string[] _interstitialAdUnits  = { "24534e1901884e398f1253216226017e" };
    private readonly string[] _rewardedAdUnits      = { "920b6145fb1546cf8b5cf2ac34638bb7", "7e756ab79a29438f87bc5c6d6032d31f" };
#endif
    enum adState
    {
        State_None = 0,
        State_Loading = 1,
        State_Suee = 2,
    }

    public enum ADLoadMode
    {
        LoadMode_Auto,
        LoadMode_Manual
    }
    //存储广告状态信息,第一个为广告ID，第二个为状态，0为无，1为请求中，2为缓存成功
    private readonly Dictionary<string, adState> _adUnitToLoadedMapping = new Dictionary<string, adState>();
    //奖励视频回调
    private Action<bool> actionRewardResult = null;

    //是否自动拉去广告
    public ADLoadMode _adLoadMode = ADLoadMode.LoadMode_Auto;
    public float _AutoLoadAdsTime       = 15;
    public MoPub.AdPosition _adPosition  = MoPubBase.AdPosition.BottomCenter;
    public MoPubBase.MaxAdSize _adSize   = MoPubBase.MaxAdSize.Width300Height50;

    private float _fLastLoadAdsTime = 0;
    private void Awake()
    {
        if (instance == null)
        {
            Instance = this;
        }

    }

    private void Start()
    {   
        MoPub.LoadBannerPluginsForAdUnits(_bannerAdUnits);
        MoPub.LoadInterstitialPluginsForAdUnits(_interstitialAdUnits);
        MoPub.LoadRewardedVideoPluginsForAdUnits(_rewardedAdUnits);


    }
    public void OnSdkInitialized(string adUnitId)
    {
        //UpdateConsentValues();
        LoadAllAds();
        _fLastLoadAdsTime = Time.realtimeSinceStartup;
    }

    private void OnEnable()
    {
        MoPubManager.OnAdLoadedEvent += OnAdLoadedEvent;
        MoPubManager.OnAdFailedEvent += OnAdFailedEvent;
        MoPubManager.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
        MoPubManager.OnInterstitialFailedEvent += OnInterstitialFailedEvent;
        MoPubManager.OnInterstitialDismissedEvent += OnInterstitialDismissedEvent;
        MoPubManager.OnRewardedVideoLoadedEvent += OnRewardedVideoLoadedEvent;
        MoPubManager.OnRewardedVideoFailedEvent += OnRewardedVideoFailedEvent;
        MoPubManager.OnRewardedVideoFailedToPlayEvent += OnRewardedVideoFailedToPlayEvent;
        MoPubManager.OnRewardedVideoClosedEvent += OnRewardedVideoClosedEvent;
        MoPubManager.OnSdkInitializedEvent += OnSdkInitialized;
    }

    private void OnDisable()
    {
        MoPubManager.OnAdLoadedEvent -= OnAdLoadedEvent;
        MoPubManager.OnAdFailedEvent -= OnAdFailedEvent;
        MoPubManager.OnInterstitialLoadedEvent -= OnInterstitialLoadedEvent;
        MoPubManager.OnInterstitialFailedEvent -= OnInterstitialFailedEvent;
        MoPubManager.OnInterstitialDismissedEvent -= OnInterstitialDismissedEvent;
        MoPubManager.OnRewardedVideoLoadedEvent -= OnRewardedVideoLoadedEvent;
        MoPubManager.OnRewardedVideoFailedEvent -= OnRewardedVideoFailedEvent;
        MoPubManager.OnRewardedVideoFailedToPlayEvent -= OnRewardedVideoFailedToPlayEvent;
        MoPubManager.OnRewardedVideoClosedEvent -= OnRewardedVideoClosedEvent;
        MoPubManager.OnSdkInitializedEvent -= OnSdkInitialized;
    }
    #region banner相关

    public bool IsBannerReady()
    {
        for (int i = 0; i < _bannerAdUnits.Length; ++i)
        {
            adState st = adState.State_None;
            if (_adUnitToLoadedMapping.TryGetValue(_bannerAdUnits[i], out st) == true)
            {
                if (st == adState.State_Suee)
                {
                    return true;
                }
            }
        }   
        return false;
    }
    //Banner相关操作
    public void OnRequireBanner(MoPub.AdPosition adPosition, MoPubBase.MaxAdSize adSize,bool bForceRequire = false)
    {
        //为了防止异常，这里应该加一个时限，超过多少秒，则忽略这两个选项，直接进行拉取
        for (int i = 0; i < _bannerAdUnits.Length; ++i)
        {
            adState st = adState.State_None;
            if (_adUnitToLoadedMapping.TryGetValue(_bannerAdUnits[i],out st) == true && bForceRequire == false)
            {
                if (st != adState.State_None) continue;
            }
            MoPub.RequestBanner(_bannerAdUnits[i], adPosition, adSize);
            _adUnitToLoadedMapping[_bannerAdUnits[i]] = adState.State_Loading;
        }
    }
    public bool OnShowBanner()
    {
        for (int i = 0; i < _bannerAdUnits.Length; ++i)
        {
            adState st = adState.State_None;
            if (_adUnitToLoadedMapping.TryGetValue(_bannerAdUnits[i], out st) == true)
            {
                if (st == adState.State_Suee)
                {
                    MoPub.ShowBanner(_bannerAdUnits[i], true);
                    return true;
                }
            }
        }
        return false;
    }
    public void OnHideBanner()
    {
        for (int i = 0; i < _bannerAdUnits.Length; ++i)
        {
            MoPub.ShowBanner(_bannerAdUnits[i], false);
        }
    }
    public void OnDestroyBanner()
    {
        for (int i = 0; i < _bannerAdUnits.Length; ++i)
        {
            MoPub.DestroyBanner(_bannerAdUnits[i]);
            _adUnitToLoadedMapping[_bannerAdUnits[i]] = adState.State_None;
        }
    }
    // Banner Events
    private void OnAdLoadedEvent(string adUnitId, float height)
    {
        _adUnitToLoadedMapping[adUnitId] = adState.State_Suee;
        MoPub.ShowBanner(adUnitId, false);
    }
    private void OnAdFailedEvent(string adUnitId, string error)
    {
        _adUnitToLoadedMapping[adUnitId] = adState.State_None;
        Debug.Log("MopubAdsManager load banner failed reason =" + error);
    }
    #endregion

    #region 插屏广告相关

    public bool IsInterstitialReady()
    {
        for (int i = 0; i < _interstitialAdUnits.Length; ++i)
        {
            adState st = adState.State_None;
            if (_adUnitToLoadedMapping.TryGetValue(_interstitialAdUnits[i], out st) == true)
            {
                if (st == adState.State_Suee)
                {
                    return true;
                }

            }
        }
        return false;
    }
    public void OnRequireInterstitial(bool bForceRequire = false)
    {
        for (int i = 0; i < _interstitialAdUnits.Length; ++i)
        {
            adState st = adState.State_None;
            if (_adUnitToLoadedMapping.TryGetValue(_interstitialAdUnits[i], out st) == true && bForceRequire == false)
            {
                if (st != adState.State_None) continue;
            }
            if (MoPub.IsInterstitialReady(_interstitialAdUnits[i]) == true)
            {
                _adUnitToLoadedMapping[_interstitialAdUnits[i]] = adState.State_Suee;
                continue;
            }
            MoPub.RequestInterstitialAd(_interstitialAdUnits[i]);
            _adUnitToLoadedMapping[_interstitialAdUnits[i]] = adState.State_Loading;
        }
    }
    public bool OnShowInterstitial()
    {
        for (int i = 0; i < _interstitialAdUnits.Length; ++i)
        {
            adState st = adState.State_None;
            if (_adUnitToLoadedMapping.TryGetValue(_interstitialAdUnits[i], out st) == true)
            {
                if (MoPub.IsInterstitialReady(_interstitialAdUnits[i]) == true)
                {
                    MoPub.ShowInterstitialAd(_interstitialAdUnits[i]);
                    _adUnitToLoadedMapping[_interstitialAdUnits[i]] = adState.State_None;
                    return true;
                }
                if (st == adState.State_Suee)
                {
                    _adUnitToLoadedMapping[_interstitialAdUnits[i]] = adState.State_None;
                    Debug.LogError("MopubAdsManager interstitial state no equal Mopub state error");
                }
            }
        }
        return false;
    }
    public void OnDestroyInterstitial()
    {
        for (int i = 0; i < _interstitialAdUnits.Length; ++i)
        {
            MoPub.DestroyInterstitialAd(_interstitialAdUnits[i]);
            _adUnitToLoadedMapping[_interstitialAdUnits[i]] = adState.State_None;
        }
    }
    // Interstitial Events
    private void OnInterstitialLoadedEvent(string adUnitId)
    {
        _adUnitToLoadedMapping[adUnitId] = adState.State_Suee;
    }
    private void OnInterstitialFailedEvent(string adUnitId, string error)
    {
        _adUnitToLoadedMapping[adUnitId] = adState.State_None;
        Debug.Log("MopubAdsManager Faile Load ads =" + error);
    }
    private void OnInterstitialDismissedEvent(string adUnitId)
    {
        _adUnitToLoadedMapping[adUnitId] = adState.State_None;
        Debug.Log("MopubAdsManager 拉去插屏广告被拒绝");
    }
    #endregion

    #region 激励广告相关
    public bool IsRewardVideoReady()
    {
        for (int i = 0; i < _rewardedAdUnits.Length; ++i)
        {
            adState st = adState.State_None;
            if (_adUnitToLoadedMapping.TryGetValue(_rewardedAdUnits[i], out st) == true)
            {
                if (st == adState.State_Suee)
                {
                    return true;
                }
                //if (MoPub.HasRewardedVideo(_rewardedAdUnits[i]) == true)
                //{
                //    return true;
                //}
            }
        }
        return false;
    }
    public void OnRequireRewardVideo(bool bForceRequire = false)
    {
        for (int i = 0; i < _rewardedAdUnits.Length; ++i)
        {
            adState st = adState.State_None;
            if (_adUnitToLoadedMapping.TryGetValue(_rewardedAdUnits[i], out st) == true && bForceRequire == false)
            {
                if (st != adState.State_None) continue;
            }
            if (MoPub.HasRewardedVideo(_rewardedAdUnits[i]) == true)
            {
                _adUnitToLoadedMapping[_rewardedAdUnits[i]] = adState.State_Suee;
                continue;
            }
            MoPub.RequestRewardedVideo(_rewardedAdUnits[i]);
            _adUnitToLoadedMapping[_rewardedAdUnits[i]] = adState.State_Loading;
        }
    }

    public bool OnShowRewardVideo(Action<bool> callback)
    {
        actionRewardResult = callback;
        for (int i = 0; i < _rewardedAdUnits.Length; ++i)
        {
            adState st = adState.State_None;
            if (_adUnitToLoadedMapping.TryGetValue(_rewardedAdUnits[i], out st) == true)
            {
                if (st == adState.State_Suee)//MoPub.HasRewardedVideo(_rewardedAdUnits[i]) == true)
                {
                    MoPub.ShowRewardedVideo(_rewardedAdUnits[i]);
                    _adUnitToLoadedMapping[_rewardedAdUnits[i]] = adState.State_None;
                    return true;
                }
                //if (st == adState.State_Suee)
                //{
                //    _adUnitToLoadedMapping[_rewardedAdUnits[i]] = adState.State_None;
                //    Debug.LogError("MopubAdsManager interstitial state no equal Mopub state error");
                //}
            }
        }
        if (actionRewardResult != null)
        {
            actionRewardResult(false);
            actionRewardResult = null;
        }
        return false;
    }
    // Rewarded Video Events
    private void OnRewardedVideoLoadedEvent(string adUnitId)
    {
        _adUnitToLoadedMapping[adUnitId] = adState.State_Suee;
    }


    private void OnRewardedVideoFailedEvent(string adUnitId, string error)
    {
        _adUnitToLoadedMapping[adUnitId] = adState.State_None;
    }


    private void OnRewardedVideoFailedToPlayEvent(string adUnitId, string error)
    {
        _adUnitToLoadedMapping[adUnitId] = adState.State_None;
        if (actionRewardResult != null)
        {
            actionRewardResult(false);
            actionRewardResult = null;
        }
    }


    private void OnRewardedVideoClosedEvent(string adUnitId)
    {
        _adUnitToLoadedMapping[adUnitId] = adState.State_None;
        if (actionRewardResult != null)
        {
            actionRewardResult(true);
            actionRewardResult = null;
        }
    }
    #endregion

    public void LoadAllAds(bool bIsForce = false)
    {
        OnRequireBanner(_adPosition, _adSize, bIsForce);
        OnRequireInterstitial(bIsForce);
        OnRequireRewardVideo(bIsForce);
    }

    private void Update()
    {
        if (_adLoadMode == ADLoadMode.LoadMode_Auto)
        {
            if ((Time.realtimeSinceStartup - _fLastLoadAdsTime) > _AutoLoadAdsTime)
            {
                _fLastLoadAdsTime = Time.realtimeSinceStartup;
                LoadAllAds();
            }
        }
    }
}
