using System.Collections.Generic;

public class FyberPackageConfig : PackageConfig
{
    public override string Name
    {
        get { return "Fyber"; }
    }

    public override string Version
    {
        get { return /*UNITY_PACKAGE_VERSION*/"1.0.5"; }
    }

    public override Dictionary<Platform, string> NetworkSdkVersions
    {
        get {
            return new Dictionary<Platform, string> {
                { Platform.ANDROID, /*ANDROID_SDK_VERSION*/"7.8.3" },
                { Platform.IOS, /*IOS_SDK_VERSION*/"7.8.7" }
            };
        }
    }

    public override Dictionary<Platform, string> AdapterClassNames
    {
        get {
            return new Dictionary<Platform, string> {
                { Platform.ANDROID, "com.mopub.mobileads.Fyber" },
                { Platform.IOS, "Fyber" }
            };
        }
    }
}
