﻿#if mopub_manager
using UnityEngine;

public class FyberNetworkConfig : MoPubNetworkConfig
{
    public override string AdapterConfigurationClassName
    {
        get { return Application.platform == RuntimePlatform.Android
                  ? "com.mopub.mobileads.FyberAdapterConfiguration"
                  : "FyberAdapterConfiguration"; }
    }

    [Tooltip("Enter your app ID to be used to initialize the Fyber SDK.")]
    [Config.Optional]
    public PlatformSpecificString appID;

}
#endif
