// filename BuildPostProcessor.cs
// put it in a folder Assets/Editor/
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;
using System.Xml;
using System.Diagnostics;
using System.Text;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;

public class BuildPostProcessor
{
    //must be between 40 and 50 to ensure that it's not overriden by Podfile generation (40) and that it's added before "pod install" (50)
    [PostProcessBuildAttribute(49)]
    private static void PostProcessBuild_iOS(BuildTarget target, string buildPath)
    {
        if (target == BuildTarget.iOS)
        {
            WriteCocoaPadInfo(buildPath);
            //string str = string.Empty;
            //StringBuilder sb = new StringBuilder();
            ////剔除facebook 构建podfile 生成的use_frameworks!
            //string podfilePath = buildPath + "/Podfile";
            //using (StreamReader sr = new StreamReader(podfilePath, Encoding.UTF8))
            //{
            //    while ((str = sr.ReadLine()) != null)
            //    {
            //        if (str.Equals("use_frameworks!"))
            //        {
            //            str = "";
            //        }
            //        sb.Append(str);
            //        sb.Append("\r\n");
            //    }
            //}
            //File.WriteAllText(podfilePath, sb.ToString());
        }
    }
    [PostProcessBuild(int.MaxValue)]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string path)
    {

        if (buildTarget == BuildTarget.iOS)
        {
            //Debug.LogError("shengming change x code plist");
            string plistPath = path + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromFile(plistPath);

            PlistElementDict rootDict = plist.root;

            rootDict.SetString("GADApplicationIdentifier", "ca-app-pub-3940256099942544~1458002511");
            PlistElementArray pld = rootDict.CreateArray("SKAdNetworkItems");

            List<string> skadnetworkList = GetAllSkAdNetWorkConfig();
            for (int i = 0; i < skadnetworkList.Count; i++)
            {
                //UnityEngine.Debug.LogError("shengming sdaniden ="+ skadnetworkList[i]);
                pld.AddDict().SetString("SKAdNetworkIdentifier", skadnetworkList[i]);
            }

            plist.WriteToFile(plistPath);
            //WriteCocoaPadInfo(path);

            //Process p = new Process();
            //p.StartInfo.FileName = "/bin/bash";
            //p.StartInfo.UseShellExecute = false;
            //p.StartInfo.RedirectStandardInput = true;
            //p.StartInfo.RedirectStandardOutput = true;
            //p.StartInfo.RedirectStandardError = true;
            //p.StartInfo.CreateNoWindow = true;
            //p.Start();
            //p.StandardInput.WriteLine("cd "+path);
            //p.StandardInput.WriteLine("pod install");
            //p.StandardInput.WriteLine("exit");
            //p.StandardInput.AutoFlush = true;

            //string strOuput = p.StandardOutput.ReadToEnd();
            //p.WaitForExit();
            //p.Close();
            //UnityEngine.Debug.Log(strOuput);

            //File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
    public static void WriteCocoaPadInfo(string path)
    {
        string pPodPath = path + "/Podfile";
        string fileInfo = System.IO.File.ReadAllText(pPodPath);
        string pAppendPodPath = path + "/../cocoapadFaceBook";
        string appendFileInfo = System.IO.File.ReadAllText(pAppendPodPath);

        string AllInfo = fileInfo + "\r\n" + appendFileInfo;
        UnityEngine.Debug.LogError("path ="+pPodPath);
        UnityEngine.Debug.LogError("AllInfo ="+AllInfo);
        System.IO.File.WriteAllText(pPodPath, AllInfo);
    }
    public static List<string> GetAllSkAdNetWorkConfig()
    {
        string path = System.IO.Directory.GetCurrentDirectory() + "/SkAdNetworkConfig";
        DirectoryInfo root = new DirectoryInfo(path);

        Dictionary<string, object> strDic = new Dictionary<string, object>();
        foreach (FileInfo f in root.GetFiles())
        {
            string filePath = $"{path}/{f.Name}";
            if (f.Extension == ".xml")
            {
                Dictionary<string, object> dic = GetSkAdNetWorkConfig(filePath);
                //strDic = GTA.Utils.MergeDictionary(strDic, dic);
                foreach (var item in dic)
                {
                    strDic[item.Key] = item.Value;
                }
            }
        }

        List<string> strList = new List<string>();
        foreach (var item in strDic)
        {
            strList.Add(item.Key);
        }
        return strList;
    }
    /// <summary>
    /// 解析单个xml 文件
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    private static Dictionary<string, object> GetSkAdNetWorkConfig(string filePath)
    {
        Dictionary<string, object> strDic = new Dictionary<string, object>();
        //Debug.Log(filePath);
        if (File.Exists(filePath))
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filePath);
            XmlNodeList node = xmlDoc.SelectSingleNode("plist").ChildNodes;
            foreach (XmlElement nodeList in node)
            {
                foreach (XmlElement xe in nodeList)
                {
                    foreach (XmlNode xe1 in xe)
                    {
                        if (xe1.Name == "dict")
                        {
                            foreach (XmlNode xe2 in xe1)
                            {
                                if (xe2.Name == "string")
                                {
                                    if (!strDic.ContainsKey(xe2.InnerText))
                                    {
                                        strDic.Add(xe2.InnerText, 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return strDic;
    }


}
#endif