using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestAdsManager : MonoBehaviour
{
    public Button DestoryBanner;
    public Button ShowBanner;
    public Button HideBanner;
    public Button Inter;
    public Button Reward_1;
    public Button Reward_2;
    // Start is called before the first frame update
    void Start()
    {
        DestoryBanner.interactable     = false;
        ShowBanner.interactable = false;
        HideBanner.interactable = false;
        Inter.interactable      = false;
        Reward_1.interactable   = false;
        Reward_2.interactable   = false;

        DestoryBanner.onClick.AddListener(DestoryBannerClick);
        ShowBanner.onClick.AddListener(ShowBannerClick);
        HideBanner.onClick.AddListener(HideBannerClick);
        Inter.onClick.AddListener(ShowInterClick);
        Reward_1.onClick.AddListener(ShowRewardVedio);
        Reward_2.onClick.AddListener(ShowRewardVedio);
    }
    void DestoryBannerClick()
    {
        MopubAdsManager.Instance.OnDestroyBanner();
    }
    void ShowBannerClick()
    {
        MopubAdsManager.Instance.OnShowBanner();
    }
    void HideBannerClick()
    {
        MopubAdsManager.Instance.OnHideBanner();
    }

    void ShowInterClick()
    {
        MopubAdsManager.Instance.OnShowInterstitial();
    }

    void ShowRewardVedio()
    {
        MopubAdsManager.Instance.OnShowRewardVideo(RewardCallBack);
    }
    void RewardCallBack(bool bFlag)
    {
        if (bFlag == true)
        {
            Debug.LogError("shengming 展示广告成功");
        }
        else
        {
            Debug.LogError("shengming 展示广告失败");
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (MopubAdsManager.Instance.IsBannerReady() == true)
        {
            ShowBanner.interactable = true;
            HideBanner.interactable = true;
            DestoryBanner.interactable = true;
        }
        else
        {
            ShowBanner.interactable = false;
            HideBanner.interactable = false;
            DestoryBanner.interactable = false;
        }

        if (MopubAdsManager.Instance.IsInterstitialReady() == true)
        {
            Inter.interactable = true;
        }
        else
        {
            Inter.interactable = false;
        }

        if (MopubAdsManager.Instance.IsRewardVideoReady() == true)
        {
            Reward_1.interactable = true;
            Reward_2.interactable = true;
        }
        else
        {
            Reward_1.interactable = false;
            Reward_2.interactable = false;
        }
    }
}
